package pl.mikolajbogutczak.gymplanner.util

import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import pl.mikolajbogutczak.gymplanner.R

fun ImageView.fromUrl(
    url: String?,
    @DrawableRes error: Int = R.color.grey,
    @DrawableRes placeHolder: Int = R.color.grey
) {
    if (url.isNullOrBlank()) {
        this.setImageResource(placeHolder)
    } else {
        Glide.with(this)
            .load(url)
            .error(ContextCompat.getDrawable(this.context, error))
            .placeholder(ContextCompat.getDrawable(this.context, placeHolder))
            .optionalFitCenter()
            .into(this)
    }
}
