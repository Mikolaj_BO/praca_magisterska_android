package pl.mikolajbogutczak.gymplanner.util

data class Resource<out ResourceType>(
    val status: ResourceStatus,
    val value: ResourceType?,
    val error: ResourceError?,
    val cache: Boolean = false
) {

    fun isSuccess(): Boolean =
        status == ResourceStatus.Success

    fun isError(): Boolean =
        status == ResourceStatus.Error

    fun isLoading(): Boolean =
        status == ResourceStatus.Loading

    companion object {
        fun <Type> loading(): Resource<Type> =
            Resource(
                ResourceStatus.Loading,
                null,
                null
            )

        fun <Type> error(error: ResourceError): Resource<Type> =
            Resource(
                ResourceStatus.Error,
                null,
                error
            )

        fun <Type> success(value: Type? = null, cache: Boolean = false): Resource<Type> =
            Resource(ResourceStatus.Success, value, null, cache)

        fun from(vararg resources: Resource<*>): Resource<Void> =
            when {
                resources.any { it.isError() } -> error(ResourceError.GenericError)
                resources.any { it.isLoading() } -> loading()
                resources.all { it.isSuccess() } -> success()
                else -> error(ResourceError.GenericError)
            }

        fun <From, To> changeType(resource: Resource<From>, value: To?): Resource<To> =
            when {
                resource.isLoading() -> loading()
                resource.isSuccess() -> success(value, resource.cache)
                else -> error(resource.error.orGeneric())
            }
    }
}

enum class ResourceStatus {
    Loading, Error, Success
}

sealed class ResourceError {
//    data class ApiError(
//        val code: ErrorCodeStatusDto?,
//        val details: String?,
//        val httpCode: Int?,
//        val error: ErrorDto? = null
//    ) : ResourceError()

    data class ErrorWithMessage(
        val details: String?,
    ) : ResourceError()

    object GenericError : ResourceError()

//    val message: String?
//        get() = when (this) {
//            is ApiError -> {
//
//                if ((this.httpCode ?: 0) < SOAP_ERRORS_HTTP_CODE) {
//                    details?.takeIf { it.isNotBlank() }
//                } else {
//                    null
//                }
//            }
//            else -> null
//        }

    companion object {
        const val SOAP_ERRORS_HTTP_CODE = 500
    }
}

fun ResourceError?.orGeneric(): ResourceError {
    return this ?: ResourceError.GenericError
}

fun <T, M> Result<T>.toResource(mapped: (input: T?) -> M): Resource<M> {
    return if (isFailure) {
        Resource.error(ResourceError.ErrorWithMessage(exceptionOrNull()?.message.orEmpty()))
    } else {
        Resource.success(mapped(getOrNull()))
    }
}

fun <T> Result<T>.toResource(): Resource<T> {
    return if (isFailure) {
        Resource.error(ResourceError.ErrorWithMessage(exceptionOrNull()?.message.orEmpty()))
    } else {
        Resource.success(getOrNull())
    }
}