package pl.mikolajbogutczak.gymplanner.util

import android.os.Handler
import android.view.View
import androidx.core.view.isVisible

fun View.gone() {
    this.visibility = View.GONE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.changeVisibility(visible: Boolean, onHidden: Int = View.GONE) {
    this.visibility = when {
        visible -> View.VISIBLE
        else -> onHidden
    }
}

fun View.changeVisibilityDelayed(visible: Boolean, delay: Long, onHidden: Int = View.GONE) {
    Handler(this.context.mainLooper).postDelayed({
        changeVisibility(visible, onHidden)
    }, delay)
}

fun View.toggleVisibility(onHidden: Int = View.GONE) {
    this.visibility = if (this.isVisible) {
        onHidden
    } else {
        View.VISIBLE
    }
}