package pl.mikolajbogutczak.gymplanner.di.data

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import pl.mikolajbogutczak.gymplanner.db.AppDatabase
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "gymplanner.db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun providePlanDao(appDatabase: AppDatabase) = appDatabase.planDao()
}