package pl.mikolajbogutczak.gymplanner.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import pl.mikolajbogutczak.gymplanner.usecase.*

@Module
@InstallIn(ActivityComponent::class)
abstract class UseCasesModule {

    @Binds
    abstract fun bindAuthenticationUseCase(
        authenticationImpl: AuthenticationImpl
    ): AuthenticationUseCases

    @Binds
    abstract fun bindExerciseUseCases(
        exercisesImpl: ExercisesImpl
    ): ExerciseUseCases

    @Binds
    abstract fun bindPlanUseCases(
        planImpl: PlanImpl
    ): PlanUseCases
}