package pl.mikolajbogutczak.gymplanner.usecase.model


data class Plan(
    var name: String? = null,
    var days: List<String> = emptyList(),
    var firebaseId: String? = null,
    val exercises: List<Exercise>,
)