package pl.mikolajbogutczak.gymplanner.usecase

import androidx.lifecycle.LiveData
import pl.mikolajbogutczak.gymplanner.db.PlanEntity
import pl.mikolajbogutczak.gymplanner.usecase.model.Plan
import pl.mikolajbogutczak.gymplanner.util.Resource

interface PlanUseCases {
    suspend fun saveInFirebase(vararg plan: Plan): Resource<Unit>
    suspend fun saveInDb(vararg plan: Plan)
    suspend fun updatePlans()
    fun getPlansFromDb(): LiveData<List<PlanEntity>>
}