package pl.mikolajbogutczak.gymplanner.usecase

import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import pl.mikolajbogutczak.gymplanner.usecase.model.Exercise
import pl.mikolajbogutczak.gymplanner.util.Resource
import pl.mikolajbogutczak.gymplanner.util.toResource
import javax.inject.Inject

class ExercisesImpl @Inject constructor() : ExerciseUseCases {

    private val firebaseDb = Firebase.database.reference

    override suspend fun fetchExercises(group: String?): Resource<List<Exercise>> {
        val result = runCatching {
            firebaseDb.get().await()
        }
        return result.toResource { input ->
            input?.child("Exercises")
                ?.children
                ?.mapNotNull {
                    it.getValue(Exercise::class.java).apply { this?.Id = it.key?.toInt() }
                }
                .orEmpty()
        }
    }

}