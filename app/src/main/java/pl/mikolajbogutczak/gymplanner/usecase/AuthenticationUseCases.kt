package pl.mikolajbogutczak.gymplanner.usecase

import pl.mikolajbogutczak.gymplanner.usecase.model.LoginResultUser
import pl.mikolajbogutczak.gymplanner.util.Resource

interface AuthenticationUseCases {
    suspend fun loginWithMail(email: String, password: String): Resource<LoginResultUser>
    suspend fun loginByFacebook()
    suspend fun loginByGoogle(token: String): Resource<LoginResultUser>
}