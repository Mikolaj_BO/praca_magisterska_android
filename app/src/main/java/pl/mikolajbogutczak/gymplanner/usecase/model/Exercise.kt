package pl.mikolajbogutczak.gymplanner.usecase.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Exercise(
    var Id: Int? = null,
    val name: String? = "",
    val picture: String? = "",
    val group: String? = "",
    val description: String? = "",
    val reps: Int? = null,
    val series: Int? = null
)