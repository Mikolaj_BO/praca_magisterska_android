package pl.mikolajbogutczak.gymplanner.usecase.model

data class LoginResultUser(
    val name: String
)