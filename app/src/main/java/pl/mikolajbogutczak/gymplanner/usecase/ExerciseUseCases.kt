package pl.mikolajbogutczak.gymplanner.usecase

import pl.mikolajbogutczak.gymplanner.usecase.model.Exercise
import pl.mikolajbogutczak.gymplanner.util.Resource

interface ExerciseUseCases {
    suspend fun fetchExercises(group: String? = null): Resource<List<Exercise>>
}