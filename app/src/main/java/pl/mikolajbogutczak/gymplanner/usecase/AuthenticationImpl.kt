package pl.mikolajbogutczak.gymplanner.usecase

import android.util.Log
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import pl.mikolajbogutczak.gymplanner.usecase.model.LoginResultUser
import pl.mikolajbogutczak.gymplanner.util.Resource
import pl.mikolajbogutczak.gymplanner.util.toResource
import javax.inject.Inject

class AuthenticationImpl @Inject constructor() : AuthenticationUseCases {

    val firebaseAuth = Firebase.auth

    override suspend fun loginWithMail(email: String, password: String): Resource<LoginResultUser> {
        val result = runCatching {
            firebaseAuth.signInWithEmailAndPassword(email, password).await()
        }
        return result.toResource { input -> LoginResultUser(input?.user?.email.orEmpty()) }
    }

    override suspend fun loginByFacebook() {
        Log.d("asdd", "asd")
    }

    override suspend fun loginByGoogle(token: String): Resource<LoginResultUser> {
        val credential = GoogleAuthProvider.getCredential(token, null)
        val result = runCatching {
            firebaseAuth.signInWithCredential(credential).await()
        }
        return result.toResource { input -> LoginResultUser(input?.user?.email.orEmpty()) }
    }

}