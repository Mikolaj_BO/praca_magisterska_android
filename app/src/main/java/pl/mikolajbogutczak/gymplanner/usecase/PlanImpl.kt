package pl.mikolajbogutczak.gymplanner.usecase

import androidx.lifecycle.LiveData
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import pl.mikolajbogutczak.gymplanner.db.PlanEntity
import pl.mikolajbogutczak.gymplanner.db.dao.PlanDao
import pl.mikolajbogutczak.gymplanner.usecase.model.Plan
import pl.mikolajbogutczak.gymplanner.util.Resource
import pl.mikolajbogutczak.gymplanner.util.toResource
import javax.inject.Inject

class PlanImpl @Inject constructor(private val planDao: PlanDao) : PlanUseCases {

    val firebaseAuth = Firebase.database.reference

    override suspend fun saveInFirebase(vararg plan: Plan): Resource<Unit> {
        saveInDb(*plan)
        val plansInDb = planDao.getAllPlans()

        val result = runCatching {
            plansInDb.forEach {
                val firebaseId = it.firebaseId ?: firebaseAuth.push().key
                it.firebaseId = firebaseId
                firebaseAuth.child("Plans").child(firebaseId.orEmpty()).setValue(it).await()
            }
        }
        planDao.updatePlans(*plansInDb.toTypedArray())
        return result.toResource()
    }

    override suspend fun updatePlans() {
        val result = runCatching {
            firebaseAuth.get().await()
        }
        val plansFromFirebase = result.toResource { input ->
            input?.child("Plans")
                ?.children
                ?.mapNotNull { it.getValue(PlanEntity::class.java).apply { this?.id = null } }
                .orEmpty()
        }
        if (plansFromFirebase.isSuccess()) {
            planDao.deleteAll()
            planDao.insertPlans(*plansFromFirebase.value.orEmpty().toTypedArray())
        }
    }

    override suspend fun saveInDb(vararg plan: Plan) {
        val planEntities = plan.map {
            PlanEntity(
                firebaseId = it.firebaseId,
                name = it.name.orEmpty(),
                days = it.days.joinToString(","),
                exercises = it.exercises
            )
        }
        planDao.insertPlans(*planEntities.toTypedArray())
    }

    override fun getPlansFromDb(): LiveData<List<PlanEntity>> {
        return planDao.getAllPlansLV()
    }
}