package pl.mikolajbogutczak.gymplanner.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.firebase.database.IgnoreExtraProperties
import pl.mikolajbogutczak.gymplanner.db.converters.ExercisesConverters
import pl.mikolajbogutczak.gymplanner.usecase.model.Exercise

@IgnoreExtraProperties
@Entity(tableName = "Plans")
@TypeConverters(
    ExercisesConverters::class,
)
data class PlanEntity(
    @PrimaryKey
    var id: Int? = null,
    var firebaseId: String? = null,
    val name: String? = null,
    val days: String? = null,
    val exercises: List<Exercise>? = null
)