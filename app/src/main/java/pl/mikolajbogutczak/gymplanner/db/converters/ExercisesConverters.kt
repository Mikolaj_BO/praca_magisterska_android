package pl.mikolajbogutczak.gymplanner.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import pl.mikolajbogutczak.gymplanner.usecase.model.Exercise

class ExercisesConverters {
    @TypeConverter
    fun exerciseToString(ratings: List<Exercise>?): String? {
        return Gson().toJson(ratings)
    }

    @TypeConverter
    fun stringToExercises(ratingJson: String?): List<Exercise>? {
        val listType = object : TypeToken<List<Exercise>>() {}.type
        return Gson().fromJson(ratingJson, listType)
    }
}