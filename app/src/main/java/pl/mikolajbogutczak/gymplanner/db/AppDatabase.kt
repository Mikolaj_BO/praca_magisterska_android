package pl.mikolajbogutczak.gymplanner.db

import androidx.room.Database
import androidx.room.RoomDatabase
import pl.mikolajbogutczak.gymplanner.db.dao.PlanDao

@Database(entities = [PlanEntity::class], version = 5)
abstract class AppDatabase : RoomDatabase() {
    abstract fun planDao(): PlanDao
}