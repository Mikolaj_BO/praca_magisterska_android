package pl.mikolajbogutczak.gymplanner.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import pl.mikolajbogutczak.gymplanner.db.PlanEntity

@Dao
interface PlanDao {

    @Query("Select * from plans")
    fun getAllPlans(): List<PlanEntity>

    @Query("Select * from plans")
    fun getAllPlansLV(): LiveData<List<PlanEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPlans(vararg plan: PlanEntity)

    @Update
    fun updatePlans(vararg plan: PlanEntity)

    @Query("DELETE FROM plans")
    fun deleteAll()
}