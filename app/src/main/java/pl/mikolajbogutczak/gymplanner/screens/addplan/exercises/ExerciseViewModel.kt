package pl.mikolajbogutczak.gymplanner.screens.addplan.exercises

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pl.mikolajbogutczak.gymplanner.usecase.ExerciseUseCases
import pl.mikolajbogutczak.gymplanner.usecase.model.Exercise
import pl.mikolajbogutczak.gymplanner.util.Resource

class ExerciseViewModel @ViewModelInject constructor(
    private val exerciseUseCases: ExerciseUseCases,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {


    val fetchExercisesResult = MutableLiveData<Resource<List<Exercise>>>()
    val exercisesAdapterModel = MutableLiveData<List<ExerciseAdapterModel>>()

    fun fetchExercises() {
        viewModelScope.launch(Dispatchers.IO) {
            val fetched = exerciseUseCases.fetchExercises()
            fetchExercisesResult.postValue(fetched)
            if (fetched.isSuccess()) {
                val mapped = fetched.value
                    ?.map {
                        ExerciseAdapterModel(
                            it.Id.toString(),
                            it.name.orEmpty(),
                            it.description.orEmpty(),
                            it.picture.orEmpty(),
                            0,
                            0,
                            false
                        )
                    }
                exercisesAdapterModel.postValue(mapped.orEmpty())
            }
        }
    }

    fun onItemClick(item: ExerciseAdapterModel) {
        val copied = exercisesAdapterModel.value?.map { it.copy() }
        copied?.find { it.name == item.name }?.opened = item.opened.not()
        exercisesAdapterModel.value = copied.orEmpty()
    }
}