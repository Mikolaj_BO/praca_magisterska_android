package pl.mikolajbogutczak.gymplanner.screens.addplan.plan

import androidx.recyclerview.widget.DiffUtil

class PlanDiffUtil(val oldList: List<PlanAdapterModel>, val newList: List<PlanAdapterModel>) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id &&
                oldList[oldItemPosition].firebaseId == newList[newItemPosition].firebaseId
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}
