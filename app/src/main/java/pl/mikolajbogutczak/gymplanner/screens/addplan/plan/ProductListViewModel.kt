package pl.mikolajbogutczak.gymplanner.screens.addplan.plan

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pl.mikolajbogutczak.gymplanner.db.PlanEntity
import pl.mikolajbogutczak.gymplanner.usecase.PlanUseCases

class ProductListViewModel @ViewModelInject constructor(
    private val planUseCases: PlanUseCases,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val plans = planUseCases.getPlansFromDb()

    val adapterModels = Transformations.map(plans) { getAdapterModels(it) }

    fun updatePlans() {
        viewModelScope.launch(Dispatchers.IO) {
            planUseCases.updatePlans()
        }
    }

    fun onItemClick(item: PlanAdapterModel): List<PlanAdapterModel> {
        val copied = adapterModels.value?.map { it.copy() }
        copied?.find { it.name == item.name }?.expanded = item.expanded.not()
        return copied.orEmpty()
    }

    private fun getAdapterModels(items: List<PlanEntity>): List<PlanAdapterModel> {
        return items.map {
            PlanAdapterModel(
                it.id ?: 0,
                it.firebaseId,
                it.name.orEmpty(),
                it.exercises.orEmpty(),
                it.days.orEmpty(),
                false
            )
        }
    }
}