package pl.mikolajbogutczak.gymplanner.screens

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import pl.mikolajbogutczak.gymplanner.R
import pl.mikolajbogutczak.gymplanner.databinding.ActivityMainBinding

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityMainBinding

    private val navController by lazy { findNavController(R.id.nav_host_fragment) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.bottomNavigation.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.page_1 -> {
                navController.navigate(R.id.action_global_trainingFragment)
                true
            }
            R.id.page_2 -> {
                navController.navigate(R.id.action_global_planFragment)
                true
            }
            R.id.page_3 -> {
                true
            }
            else -> {
                false
            }
        }
    }
}