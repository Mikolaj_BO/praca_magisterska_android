package pl.mikolajbogutczak.gymplanner.screens.addplan.plan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_plan.view.*
import pl.mikolajbogutczak.gymplanner.R
import pl.mikolajbogutczak.gymplanner.util.changeVisibility

class PlansAdapter(val withStartButton: Boolean = false) :
    RecyclerView.Adapter<PlansAdapter.PlanViewHolder>() {

    private var dataSet = listOf<PlanAdapterModel>()

    internal var onItemClick: (PlanAdapterModel) -> Unit = {}

    internal var onStartClick: (PlanAdapterModel) -> Unit = {}

    fun setData(newList: List<PlanAdapterModel>) {
        if (dataSet.isEmpty()) {
            dataSet = newList.toList()
            notifyDataSetChanged()
        } else {
            val diff = DiffUtil.calculateDiff(PlanDiffUtil(dataSet, newList))
            dataSet = newList.toList()
            diff.dispatchUpdatesTo(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlanViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_plan, parent, false)
        return PlanViewHolder(itemView)
    }

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: PlanViewHolder, position: Int) {
        holder.bind(dataSet[position], position)
    }

    inner class PlanViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

        }

        fun bind(item: PlanAdapterModel, position: Int) {
            val adapter = EmbeddedExerciseAdapter()
            itemView.exercises.adapter = adapter

            with(itemView) {

                start_plan.changeVisibility(withStartButton)
                start_plan.setOnClickListener { onStartClick(item) }

                when (position % 2 == 0) {
                    true -> rootView.setBackgroundResource(R.color.grey)
                    false -> rootView.setBackgroundResource(R.color.white)
                }

                rootView.setOnClickListener { onItemClick(item) }

                plan_name.text = item.name
                day_value.text = item.days
                adapter.setData(item.exercises)
                exercises.visibility = if (item.expanded) View.VISIBLE else View.GONE
            }
        }

    }
}