package pl.mikolajbogutczak.gymplanner.screens.addplan.exercises

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ExerciseAdapterModel(
    val id: String,
    val name: String,
    val description: String,
    val pictureUrl: String,
    var series: Int,
    var reps: Int,
    var opened: Boolean
) : Parcelable