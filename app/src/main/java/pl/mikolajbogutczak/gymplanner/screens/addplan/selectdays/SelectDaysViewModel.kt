package pl.mikolajbogutczak.gymplanner.screens.addplan.selectdays

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pl.mikolajbogutczak.gymplanner.usecase.PlanUseCases
import pl.mikolajbogutczak.gymplanner.usecase.model.Plan
import pl.mikolajbogutczak.gymplanner.util.Resource

class SelectDaysViewModel @ViewModelInject constructor(
    private val planUseCases: PlanUseCases,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val days = mutableSetOf<String>()

    val saveResource = MutableLiveData<Resource<Unit>>()

    fun addDay(day: String) {
        days.add(day)
    }

    fun saveInFirebase(plan: Plan) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = planUseCases.saveInFirebase(plan)
            saveResource.postValue(result)
        }
    }
}