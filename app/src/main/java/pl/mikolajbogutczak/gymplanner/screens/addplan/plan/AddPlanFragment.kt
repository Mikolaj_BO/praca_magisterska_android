package pl.mikolajbogutczak.gymplanner.screens.addplan.plan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pl.mikolajbogutczak.gymplanner.databinding.AddPlanFragmentBinding

@AndroidEntryPoint
class AddPlanFragment : Fragment() {

    private lateinit var binding: AddPlanFragmentBinding
    private val viewModel: ProductListViewModel by viewModels()

    private val adapter: PlansAdapter by lazy { PlansAdapter() }

    // View initialization logic
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = AddPlanFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.updatePlans()

        adapter.onItemClick = {
            val updated = viewModel.onItemClick(it)
            adapter.setData(updated)
        }
        binding.alreadyCreatedPlans.adapter = adapter

        viewModel.adapterModels.observe(viewLifecycleOwner) {
            adapter.setData(it)
        }

        binding.addNewPlan.setOnClickListener {
            val action = AddPlanFragmentDirections.actionAddPlanFragmentToSelectExercisesFragment()
            findNavController().navigate(action)
        }
    }
}