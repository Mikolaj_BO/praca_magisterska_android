package pl.mikolajbogutczak.gymplanner.screens.login

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pl.mikolajbogutczak.gymplanner.usecase.AuthenticationUseCases
import pl.mikolajbogutczak.gymplanner.usecase.model.LoginResultUser
import pl.mikolajbogutczak.gymplanner.util.Resource

class LoginViewModel @ViewModelInject constructor(
    val authenticationUseCases: AuthenticationUseCases,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {


    val loginResult = MutableLiveData<Resource<LoginResultUser>>()

    fun loginByEmailAddress(email: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = authenticationUseCases.loginWithMail(email, password)
            loginResult.postValue(result)
        }
    }

    fun loginByGoogleAddress(idToken: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = authenticationUseCases.loginByGoogle(idToken.orEmpty())
            loginResult.postValue(result)
        }
    }

    fun loginByFacebook() {

    }
}