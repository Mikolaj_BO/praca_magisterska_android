package pl.mikolajbogutczak.gymplanner.screens.addplan.selectdays

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import pl.mikolajbogutczak.gymplanner.databinding.SelectDaysFragmentBinding
import pl.mikolajbogutczak.gymplanner.screens.addplan.exercises.ExercisesAdapter
import pl.mikolajbogutczak.gymplanner.usecase.model.Exercise
import pl.mikolajbogutczak.gymplanner.usecase.model.Plan

@AndroidEntryPoint
class SelectDaysFragment : Fragment() {

    private lateinit var binding: SelectDaysFragmentBinding
    private val adapter: ExercisesAdapter by lazy { ExercisesAdapter() }

    private val args: SelectDaysFragmentArgs by navArgs()
    private val viewModel: SelectDaysViewModel by viewModels()

    // View initialization logic
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SelectDaysFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.mondayCheck.setOnClickListener { viewModel.addDay(binding.monday.text.toString()) }
        binding.tuesdayCheck.setOnClickListener { viewModel.addDay(binding.tuesday.text.toString()) }
        binding.wendesdayCheck.setOnClickListener { viewModel.addDay(binding.wendesday.text.toString()) }
        binding.thursdayCheck.setOnClickListener { viewModel.addDay(binding.wendesday2.text.toString()) }
        binding.fridayCheck.setOnClickListener { viewModel.addDay(binding.wendesday3.text.toString()) }
        binding.saturdayCheck.setOnClickListener { viewModel.addDay(binding.wendesday4.text.toString()) }
        binding.sundayCheck.setOnClickListener { viewModel.addDay(binding.wendesday5.text.toString()) }

        binding.exercises.adapter = adapter
        adapter.setData(args.newPlan?.exercises.orEmpty())
        binding.addNewExerciseButton.setOnClickListener {
            val updatedPlan = Plan(binding.planNameEdit.text.toString(),
                viewModel.days.toList(),
                null,
                args.newPlan?.exercises.orEmpty().map {
                    Exercise(
                        it.id.toInt(),
                        it.name,
                        it.pictureUrl,
                        "",
                        it.description,
                        it.reps,
                        it.series
                    )
                }
            )
            viewModel.saveInFirebase(updatedPlan)
        }

        viewModel.saveResource.observe(viewLifecycleOwner) {
            if (it.isSuccess()) {
                val actionNavigate =
                    SelectDaysFragmentDirections.actionSelectDaysFragmentToAddPlanFragment()
                findNavController().navigate(actionNavigate)
            }
        }
    }
}