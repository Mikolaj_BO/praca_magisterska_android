package pl.mikolajbogutczak.gymplanner.screens.selectplan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pl.mikolajbogutczak.gymplanner.databinding.SelectPlanFragmentBinding
import pl.mikolajbogutczak.gymplanner.screens.addplan.exercises.ExerciseAdapterModel
import pl.mikolajbogutczak.gymplanner.screens.addplan.model.Plan
import pl.mikolajbogutczak.gymplanner.screens.addplan.plan.PlansAdapter

@AndroidEntryPoint
class SelectPlanFragment : Fragment() {

    private lateinit var binding: SelectPlanFragmentBinding
    private val viewModel: SelectPlanViewModel by viewModels()

    private val adapter: PlansAdapter by lazy { PlansAdapter(true) }

    // View initialization logic
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SelectPlanFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        adapter.onItemClick = {
            val updated = viewModel.onItemClick(it)
            adapter.setData(updated)
        }

        adapter.onStartClick = {
            val exercisesAdapterModel = it.exercises.map { ex ->
                ExerciseAdapterModel(
                    ex.Id.toString(),
                    ex.name.orEmpty(),
                    ex.description.orEmpty(),
                    ex.picture.orEmpty(),
                    ex.series ?: 0,
                    ex.reps ?: 0,
                    false
                )
            }
            val plan = Plan(it.name, it.days.split(","), exercisesAdapterModel)
            val action =
                SelectPlanFragmentDirections.actionSelectPlanFragmentToPlanShowFragment(plan)
            findNavController().navigate(action)
        }

        binding.plansToSelect.adapter = adapter

        viewModel.adapterModels.observe(viewLifecycleOwner) {
            adapter.setData(it)
        }
    }
}