package pl.mikolajbogutczak.gymplanner.screens.addplan.exercises

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_exercise.view.*
import pl.mikolajbogutczak.gymplanner.R
import pl.mikolajbogutczak.gymplanner.util.fromUrl

class ExercisesAdapter : RecyclerView.Adapter<ExercisesAdapter.ExerciseViewHolder>() {

    private var dataSet = listOf<ExerciseAdapterModel>()

    internal var onItemClick: (ExerciseAdapterModel) -> Unit = {}

    fun setData(newList: List<ExerciseAdapterModel>) {
        if (dataSet.isEmpty()) {
            dataSet = newList.toList()
            notifyDataSetChanged()
        } else {
            val diff = DiffUtil.calculateDiff(ExercisesDiffUtil(dataSet, newList))
            dataSet = newList.toList()
            diff.dispatchUpdatesTo(this)
        }
    }

    fun getSelectedExercises(): List<ExerciseAdapterModel> {
        return dataSet.filter { it.series > 0 && it.reps > 0 }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_exercise, parent, false)
        return ExerciseViewHolder(itemView)
    }

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: ExerciseViewHolder, position: Int) {
        holder.bind(dataSet[position], position)
    }

    inner class ExerciseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ExerciseAdapterModel, position: Int) {

            with(itemView) {

                when (position % 2 == 0) {
                    true -> itemView.rootView.setBackgroundResource(R.color.grey)
                    false -> itemView.rootView.setBackgroundResource(R.color.white)
                }

                itemView.rootView.setOnClickListener { onItemClick(item) }

                reps_plus_button.setOnClickListener {
                    item.reps += 1
                    reps_number.text = item.reps.toString()
                }
                reps_minus_button.setOnClickListener {
                    item.reps -= 1
                    reps_number.text = item.reps.toString()
                }

                series_minus_button.setOnClickListener {
                    item.series -= 1
                    number_of_series.text = item.series.toString()
                }
                series_plus_button.setOnClickListener {
                    item.series += 1
                    number_of_series.text = item.series.toString()
                }
                number_of_series.text = item.series.toString()
                reps_number.text = item.reps.toString()
                description.visibility = if (item.opened) View.VISIBLE else View.GONE
                description.text = item.description
                exercise_name.text = item.name
                exercise_url.fromUrl(item.pictureUrl)
            }
        }

    }

    internal sealed class Payload {
        object FoldedChange : Payload()
    }
}