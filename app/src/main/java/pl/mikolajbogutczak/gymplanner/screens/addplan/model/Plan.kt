package pl.mikolajbogutczak.gymplanner.screens.addplan.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import pl.mikolajbogutczak.gymplanner.screens.addplan.exercises.ExerciseAdapterModel

@Parcelize
data class Plan(
    var name: String? = null,
    var days: List<String> = emptyList(),
    val exercises: List<ExerciseAdapterModel>,
) : Parcelable