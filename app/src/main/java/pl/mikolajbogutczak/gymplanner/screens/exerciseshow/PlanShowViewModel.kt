package pl.mikolajbogutczak.gymplanner.screens.exerciseshow

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import pl.mikolajbogutczak.gymplanner.screens.addplan.model.Plan
import pl.mikolajbogutczak.gymplanner.usecase.PlanUseCases

class PlanShowViewModel @ViewModelInject constructor(
    private val planUseCases: PlanUseCases,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val plans = planUseCases.getPlansFromDb()

    val adapterModels = MutableLiveData<List<ExerciseCoverFlowModel>>()

    fun getAdapterModels(plan: Plan) {
        adapterModels.value = plan.exercises.map { ex ->
            val series = (1..ex.series).map { Series(it, ex.reps, 0) }
            ExerciseCoverFlowModel(ex.name, ex.pictureUrl, ex.description, series)
        }
    }
}