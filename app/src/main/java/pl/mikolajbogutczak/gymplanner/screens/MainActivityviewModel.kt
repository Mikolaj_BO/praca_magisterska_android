package pl.mikolajbogutczak.gymplanner.screens

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import pl.mikolajbogutczak.gymplanner.usecase.AuthenticationUseCases

class MainActivityViewModel @ViewModelInject constructor(
    val authenticationUseCases: AuthenticationUseCases
) : ViewModel() {

    fun login() {

    }
}