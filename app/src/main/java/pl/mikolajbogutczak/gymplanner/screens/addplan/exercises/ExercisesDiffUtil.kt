package pl.mikolajbogutczak.gymplanner.screens.addplan.exercises

import androidx.recyclerview.widget.DiffUtil

class ExercisesDiffUtil(
    val oldList: List<ExerciseAdapterModel>,
    val newList: List<ExerciseAdapterModel>
) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].name == newList[newItemPosition].name
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}
