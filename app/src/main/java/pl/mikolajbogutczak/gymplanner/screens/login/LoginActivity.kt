package pl.mikolajbogutczak.gymplanner.screens.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*
import pl.mikolajbogutczak.gymplanner.contracts.GoogleSingInContract
import pl.mikolajbogutczak.gymplanner.databinding.ActivityLoginBinding
import pl.mikolajbogutczak.gymplanner.screens.MainActivity
import timber.log.Timber

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {


    private val loginViewModel: LoginViewModel by viewModels()
    private lateinit var binding: ActivityLoginBinding


    private val openGoogleStartActivityForResult =
        registerForActivityResult(GoogleSingInContract()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
                try {
                    val account = task.getResult(ApiException::class.java)!!
                    loginViewModel.loginByGoogleAddress(account.idToken)
                } catch (e: ApiException) {
                    Timber.d("Google failed")
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.singIn.setOnClickListener {
            loginViewModel.loginByEmailAddress(
                email_edit.text.toString(),
                password_edit.text.toString()
            )
        }

        binding.googleSignIn.setOnClickListener {
            openGoogleStartActivityForResult.launch(null)
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        loginViewModel.loginResult.observe(this) {
            if (it.isSuccess()) {
                //TODO: ktx?
                startActivity(Intent(this, MainActivity::class.java))
            }
        }
    }
}