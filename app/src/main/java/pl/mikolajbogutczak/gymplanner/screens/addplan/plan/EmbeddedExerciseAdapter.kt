package pl.mikolajbogutczak.gymplanner.screens.addplan.plan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_exercise2.view.*
import pl.mikolajbogutczak.gymplanner.R
import pl.mikolajbogutczak.gymplanner.usecase.model.Exercise
import pl.mikolajbogutczak.gymplanner.util.fromUrl

class EmbeddedExerciseAdapter : RecyclerView.Adapter<EmbeddedExerciseAdapter.ExerciseViewHolder>() {

    private var dataSet = listOf<Exercise>()

    fun setData(newList: List<Exercise>) {
        if (dataSet.isEmpty()) {
            dataSet = newList.toList()
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_exercise2, parent, false)
        return ExerciseViewHolder(itemView)
    }

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: ExerciseViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }

    inner class ExerciseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Exercise) {

            with(itemView) {

                exercise_url2.fromUrl(item.picture.orEmpty())
                exercise_name2.text = item.name
                series_label2.text = (item.series ?: 0).toString()
            }
        }

    }
}