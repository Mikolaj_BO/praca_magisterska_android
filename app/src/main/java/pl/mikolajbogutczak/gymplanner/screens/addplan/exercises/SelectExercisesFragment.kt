package pl.mikolajbogutczak.gymplanner.screens.addplan.exercises

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pl.mikolajbogutczak.gymplanner.databinding.SelectExerciseFragmentBinding
import pl.mikolajbogutczak.gymplanner.screens.addplan.model.Plan

@AndroidEntryPoint
class SelectExercisesFragment : Fragment() {

    private lateinit var binding: SelectExerciseFragmentBinding
    private val viewModel: ExerciseViewModel by viewModels()

    private val adapter: ExercisesAdapter by lazy { ExercisesAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.fetchExercises()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SelectExerciseFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.nextButton.setOnClickListener {
            val exercises = adapter.getSelectedExercises()
            val newPlan = Plan(exercises = exercises)
            val action =
                SelectExercisesFragmentDirections.actionSelectExercisesFragmentToSelectDaysFragment2(
                    newPlan
                )
            findNavController().navigate(action)
        }

        adapter.onItemClick = {
            viewModel.onItemClick(it)
        }
        binding.exercises.adapter = adapter

        viewModel.exercisesAdapterModel.observe(viewLifecycleOwner) {
            adapter.setData(it)
        }
    }
}