package pl.mikolajbogutczak.gymplanner.screens.training

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pl.mikolajbogutczak.gymplanner.databinding.TrainingFragmentBinding
import pl.mikolajbogutczak.gymplanner.screens.addplan.plan.EmbeddedExerciseAdapter

@AndroidEntryPoint
class TrainingFragment : Fragment() {

    private lateinit var binding: TrainingFragmentBinding
    private val viewModel: TrainingViewModel by viewModels()

    private val adapter: EmbeddedExerciseAdapter by lazy { EmbeddedExerciseAdapter() }

    // View initialization logic
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = TrainingFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

//        adapter.onItemClick = {
//            val updated = viewModel.onItemClick(it)
//            adapter.setData(updated)
//        }
//
//        adapter.onStartClick = {
//
//        }
//
//        binding.e.adapter = adapter
//
//        viewModel.adapterModels.observe(viewLifecycleOwner) {
//            adapter.setData(it)
//        }

        binding.startTrainingButton.setOnClickListener {
            val action = TrainingFragmentDirections.actionTrainingFragmentToSelectPlanFragment()
            findNavController().navigate(action)
        }

    }
}