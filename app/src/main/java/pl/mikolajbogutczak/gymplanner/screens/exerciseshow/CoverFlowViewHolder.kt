package pl.mikolajbogutczak.gymplanner.screens.exerciseshow

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_coverflow.view.*
import pl.mikolajbogutczak.gymplanner.R
import pl.mikolajbogutczak.gymplanner.util.fromUrl
import timber.log.Timber

class CoverFlowAdapter :
    RecyclerView.Adapter<CoverFlowAdapter.CoverflowViewHolder>() {

    private val dataSet: MutableList<ExerciseCoverFlowModel> = mutableListOf()

    fun setData(values: List<ExerciseCoverFlowModel>) {
        Timber.v("Set ${values.size} items")
        dataSet.clear()
        dataSet.addAll(values)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CoverFlowAdapter.CoverflowViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_coverflow, parent, false)
        return CoverflowViewHolder(itemView)
    }

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: CoverFlowAdapter.CoverflowViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }

    inner class CoverflowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ExerciseCoverFlowModel) {
            val adapter = EmbeddedSeriesAdapter()

            with(itemView) {
                series.adapter = adapter
                plan_name.text = item.name
                description2.text = item.description
                exercise_picture.fromUrl(item.picture)
                adapter.setData(item.series)
            }
        }
    }
}
