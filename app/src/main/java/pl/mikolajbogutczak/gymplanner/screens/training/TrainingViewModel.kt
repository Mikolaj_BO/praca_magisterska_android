package pl.mikolajbogutczak.gymplanner.screens.training

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import pl.mikolajbogutczak.gymplanner.db.PlanEntity
import pl.mikolajbogutczak.gymplanner.screens.addplan.plan.PlanAdapterModel
import pl.mikolajbogutczak.gymplanner.usecase.PlanUseCases

class TrainingViewModel @ViewModelInject constructor(
    val planUseCases: PlanUseCases
) : ViewModel() {

    val plans = planUseCases.getPlansFromDb()

    val adapterModels = Transformations.map(plans) { getAdapterModels(it) }

    fun onItemClick(item: PlanAdapterModel): List<PlanAdapterModel> {
        val copied = adapterModels.value?.map { it.copy() }
        copied?.find { it.name == item.name }?.expanded = item.expanded.not()
        return copied.orEmpty()
    }

    private fun getAdapterModels(items: List<PlanEntity>): List<PlanAdapterModel> {
        return items.map {
            PlanAdapterModel(
                it.id ?: 0,
                it.firebaseId,
                it.name.orEmpty(),
                it.exercises.orEmpty(),
                it.days.orEmpty(),
                false
            )
        }
    }
}
