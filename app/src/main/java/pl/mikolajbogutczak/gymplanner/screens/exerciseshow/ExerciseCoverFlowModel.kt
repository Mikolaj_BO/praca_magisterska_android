package pl.mikolajbogutczak.gymplanner.screens.exerciseshow

data class ExerciseCoverFlowModel(
    val name: String,
    val picture: String,
    val description: String,
    val series: List<Series> = emptyList()
)

data class Series(
    val series: Int,
    val reps: Int,
    val kgs: Int
)
