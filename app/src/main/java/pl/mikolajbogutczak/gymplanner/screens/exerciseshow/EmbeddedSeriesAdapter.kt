package pl.mikolajbogutczak.gymplanner.screens.exerciseshow

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_series.view.*
import pl.mikolajbogutczak.gymplanner.R

class EmbeddedSeriesAdapter : RecyclerView.Adapter<EmbeddedSeriesAdapter.SeriesViewHolder>() {

    private var dataSet = listOf<Series>()

    fun setData(newList: List<Series>) {
        if (dataSet.isEmpty()) {
            dataSet = newList.toList()
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeriesViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_series, parent, false)
        return SeriesViewHolder(itemView)
    }

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: SeriesViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }

    inner class SeriesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(item: Series) {

            with(itemView) {

                series_number.text = "${item.series} series"
                reps.text = "${item.reps} reps"
            }
        }

    }
}