package pl.mikolajbogutczak.gymplanner.screens.addplan.plan

import pl.mikolajbogutczak.gymplanner.usecase.model.Exercise

data class PlanAdapterModel(
    val id: Int,
    val firebaseId: String? = null,
    val name: String,
    val exercises: List<Exercise>,
    val days: String,
    var expanded: Boolean
)