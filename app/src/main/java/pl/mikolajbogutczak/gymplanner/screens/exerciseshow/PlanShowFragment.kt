package pl.mikolajbogutczak.gymplanner.screens.exerciseshow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import pl.mikolajbogutczak.gymplanner.databinding.PlanShowFragmentBinding

@AndroidEntryPoint
class PlanShowFragment : Fragment() {

    private lateinit var binding: PlanShowFragmentBinding
    private val viewModel: PlanShowViewModel by viewModels()

    private val adapter: CoverFlowAdapter by lazy { CoverFlowAdapter() }
    private val args: PlanShowFragmentArgs by navArgs()

    // View initialization logic
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = PlanShowFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        args.newPlan?.let { viewModel.getAdapterModels(it) }

        binding.carousel.getViewPager().offscreenPageLimit = 10

        binding.carousel.getViewPager().setPageTransformer(
            CarouselTransformer(
                orientation = binding.carousel.getViewPager().orientation,
                unSelectedItemRotation = -100f,
                unSelectedItemAlpha = 0.1f,
                minScale = 0.01f,
                itemGap = -200f
            )
        )


        binding.carousel.getViewPager().adapter = adapter
        viewModel.adapterModels.observe(viewLifecycleOwner) {
            adapter.setData(it)
        }
    }
}